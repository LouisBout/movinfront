import React, { Component } from 'react'
import { ApolloProvider } from 'react-apollo'
import { ApolloProvider as ApolloHooksProvider } from 'react-apollo-hooks'
import Client from './client'
import ThemeProvider from 'assets/theme'
import { MainNavigation } from './navigator'
import initFonts from '@/src/utils/initFonts'


export default class App extends Component{
  async componentDidMount() {
    await initFonts()
    this.setState({ fontLoaded: true })
  }
  render(){
    return this.state && this.state.fontLoaded ?
      <ApolloProvider  client={Client}>
        <ApolloHooksProvider client={Client}>
        <ThemeProvider>
          <MainNavigation />
          </ThemeProvider>
        </ApolloHooksProvider>
      </ApolloProvider>
    : null
  }
}