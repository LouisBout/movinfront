import { ApolloClient } from 'apollo-client'
import { AsyncStorage } from 'react-native'
import { HttpLink } from 'apollo-link-http'
import cache from '@/cache'
import clientStore from 'store'
import { setContext } from "apollo-link-context";

const withToken = setContext(async () => {
  let payload = await AsyncStorage.getItem('@payload')
  payload = JSON.parse(payload)
  return payload ? {
    headers: {
      authorization: `Bearer ${payload.token}`
    }
  }: null
})

let link = new HttpLink({uri:'http://localhost:4000/'})

const client = new ApolloClient({
  cache,
  connectToDevtools: true,
  link: withToken.concat(link)
})

export default client