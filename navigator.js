import { createAppContainer } from 'react-navigation'
import { createFluidNavigator } from 'react-navigation-fluid-transitions'
import * as pages from './src/pages'
import { MenuOverlay } from 'components'

export const MainNavigator = createFluidNavigator(
  {
    Home: pages.Homepage,
    MenuOverlay,
    Search: pages.Search,
    ...pages.Wizard,
    ...pages.Ask,
    ...pages.Offer,
    ...pages.Auth,
    ...pages.MySpace
  },
  {
    transitionConfig:{
      duration: 500
    }
  }
)

export const MainNavigation = createAppContainer(MainNavigator)