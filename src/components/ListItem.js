import React, { Component, Fragment } from 'react'
import { MView } from 'components'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styled  from 'styled-components/native'
import { Surface, Title as PaperTitle, Divider as PaperDivider } from 'react-native-paper'
import { styles } from 'assets/styles'



class ListItem extends Component{
  render(){
    const { width='100%', icon, roundness='5px', height='200px', type='primary', children, footer } = this.props
    const theme = {
      primary: {
        background: '#2B6171',
        color: '#F6DE18',
        icon: {
          background: '#F6DE18',
          color: '#2B6171'
        }
      },
      secondary: {
        background: '#F6DE18',
        color: '#2B6171',
        icon: {
          background: '#2B6171',
          color: '#D9EDEE'
        }
      },
      tertiary: {
        background: '#D9EDEE',
        color: '#2B6171',
        icon: {
          background: '#2B6171',
          color: '#F6DE18'
        }
      }
    }
    const ListItemContainer = styled.TouchableOpacity`
      background: ${theme[type].background};
      border-radius: ${roundness};
      color: ${theme[type].color};
      width: ${width};
      overflow: hidden;
      min-height: ${height};
    `

    const Header = styled.View`
      flex-direction: row;
      width: 100%;
      height: 60px;
      border-top-left-radius: 5px;
      border-top-right-radius: 5px;
      align-items: center;
      align-self: flex-start;
    `

    const Footer = styled.View`
      flex-direction: row;
      background: #2B6171;
      padding: 5px;
      width: 100%;
      height: 60px;
      border-bottom-left-radius: 5px;
      border-bottom-right-radius: 5px;
      align-items: center;
      align-self: flex-end;
    `

    const IconContainer = styled.View`
      border-radius: 5px;
      background: ${theme[type].icon.background};
      margin: 3px;
      justify-content: center;
      align-items: center;
      width: 54px;
      display: flex;
      height: 54px;
    `

    const Title = styled(PaperTitle)`
      display: flex;
      text-align: center;
      color: ${theme[type].color}
      font-family: SansitaOne;
      width: 250px;
      font-size: 22px;
    `
    
    const Divider = styled(PaperDivider)`
      border-color: ${theme[type].color}
      align-self: center;
      border-width: 3px;
      width: 50%;
      border-radius: 5px;
    `

    return (
      <Surface style={styles.listItemsurface}>
        <ListItemContainer {...this.props}>
          <Fragment>
            <Header>
              <IconContainer>
                <Icon name={icon} size={45} color={theme[type].icon.color}/>
              </IconContainer>
              <Title numberOfLines={1}>{this.props.title}</Title>
            </Header>
            <Divider />
            <MView fluid height="100px">
              {children}
            </MView>
            <MView fluid height="60px">
              <Footer>
                {footer}
              </Footer>
            </MView>
          </Fragment>
        </ListItemContainer>
      </Surface>
    )
  }
}

export default ListItem