import React, { Component } from 'react'
import SwitchSelector from 'react-native-switch-selector'


class MSwitch extends Component{
  render(){
    const { options, onPress, backgroundColor="#2B6171", style, buttonColor="#F6DE18", initial, selectedColor="#2B6171", height=60, fontSize=20, selectedFont="SansitaOne", globalFont="SansitaOne", globalColor="#F6DE18" } = this.props
    const selectedTextStyle = {
      fontFamily: selectedFont //F6DE18
    }
    const textStyle = {
      fontFamily: globalFont
    }
    const textColor = globalColor

    return (
      <SwitchSelector  
      fontSize={fontSize} 
      style={style}
      backgroundColor={backgroundColor}
      height={height} 
      borderRadius={5}  
      selectedTextStyle={selectedTextStyle} 
      textStyle={textStyle} 
      hasPadding={true} 
      buttonColor={buttonColor} 
      selectedColor={selectedColor} 
      textColor={textColor}
      options={options}
      initial={initial}
      onPress={onPress}
      />
    )
  }
}

export default MSwitch