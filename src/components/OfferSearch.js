import React, { Component, Fragment } from 'react'
import { MView, MButton, OfferList } from 'components'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import { graphql } from 'react-apollo'
import { compose } from 'recompose'
import { Formik } from 'formik'
import { getOffers } from 'store/offer/schema.gql'

const withData = compose(
  graphql(getOffers, { options: () => ({ variables: { orderBy: "createdAt_DESC", first: 50 } }) }),
)


class OfferSearch extends Component {
  render() {
    const { refetch, offers } = this.props.data
    return (
      <Fragment>
        <MView height="90%">
          <Formik initialValues={{ city: {} }} onSubmit={values => refetch({ where: values })}>
            {({ handleSubmit, setFieldValue }) => (
              <Fragment>
                <GooglePlacesAutocomplete
                placeholder='Entrez une ville'
                placeholderTextColor='#6e6e6e'
                minLength={2}
                autoFocus={false}
                returnKeyType={'search'}
                keyboardAppearance={'light'}
                listViewDisplayed='false'
                fetchDetails={true}
                onPress={(data) => {
                  setFieldValue('city', { address_contains: data.formatted_address || data.description  })
                }}
                query={{
                  key: 'AIzaSyD2hYIZydK-HkghQ8jCag8GO-na8PLR5Ic',
                  language: 'fr',
                  types: '(cities)'
                }}
                styles={{
                  textInputContainer: {
                    height: 60,
                    backgroundColor: '#eeeeee',
                    // flex: 1,
                    // flexGrow: 1,
                    borderRadius: 5,
                    width: '100%',
                    borderTopWidth: 0,
                    borderBottomWidth: 0
                  },
                  textInput: {
                    height: 60,
                    borderTopWidth: 0,
                    backgroundColor: '#eeeeee',
                    marginTop: 0,
                    marginLeft: 0,
                    padding: 0
                  },
                  separator: {
                    backgroundColor: '#F6DE18',
                    alignSelf: 'center',
                    height: 2,
                    width: '80%'
                  },
                  description: {
                    color: 'white',
                    fontFamily: 'Kameron-Bold'
                  }
                }}
                enablePoweredByContainer={false}
                currentLocation={true}
                currentLocationLabel="Me géolocaliser"
                nearbyPlacesAPI='GoogleReverseGeocoding'
                GoogleReverseGeocodingQuery={{
                  result_type: 'locality'
                }}
                filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
                debounce={200}
              />
                <MButton width="30%" height="40px" onPress={handleSubmit} style={{ minHeight: 40, marginTop: 15, marginBottom: 15 }}>Go</MButton>
              </Fragment>
            )}
          </Formik>
          <OfferList offers={offers} />
        </MView>
      </Fragment>
    )
  }
}
export default withData(OfferSearch)