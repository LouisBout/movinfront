import React, { Component } from 'react'
import { AsyncStorage } from 'react-native'
import { IconButton } from 'react-native-paper'
import { Transition } from 'react-navigation-fluid-transitions'
import { MView, MButton } from 'components'
import client from '@/client'

class MenuOverlay extends Component{
  state = {
    links: [
      { name: 'Accueil', action: () => this.goTo('Home') },
      { name: 'Apporter de l\'aide', action: () => this.goTo('WizardOffer') },
      { name: 'Besoin de bras', action: () => this.goTo('WizardAsk') }
    ]
  }
  async componentDidMount(){
    let isConnected = !! await AsyncStorage.getItem('@payload')
    let additionnalLinks
      if(isConnected){
        additionnalLinks = [
          { name: 'Mon espace perso', action: () => this.goTo('MySpaceHome', { module: 0 })},
          { name: 'Se déconnecter', action: () => this.logOut() }
        ]
      }else{
        additionnalLinks = [
          { name: 'S\'enregistrer', action: () => this.goTo('SignUp') },
          { name: 'Se connecter', action: () => this.goTo('LogIn') }
        ]
      }
    this.setState({
      links: [...this.state.links, ...additionnalLinks]
    })
  }
  async logOut(){
    await AsyncStorage.clear()
    this.goTo('Home')
    client.resetStore()
    
  }
  goTo(route, params){
    this.props.navigation.navigate(route, params)
  }
  render(){
    const { links } = this.state
    return (
      <Transition appear='vertical'>
          <MView style={{alignItems: 'center', justifyContent: 'center'}} fluid background="#2B6171">
            <IconButton icon="close" size={30} color="#F6DE18" onPress={() => this.props.navigation.goBack()} />
            { links && links.map( (link, idx) => 
            <MButton color="secondary"
             onPress={() => link.action()}
             key={idx}
             background="transparent" 
             height="40px">
             {link.name}
             </MButton>) }
          </MView>
        </Transition>
    )
  }
}

export default MenuOverlay