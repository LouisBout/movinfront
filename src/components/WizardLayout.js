import React, { Component } from 'react'
import { StyleSheet, ScrollView, StatusBar, Text } from 'react-native'
import { Transition } from 'react-navigation-fluid-transitions'
import * as Logo from './Logo'
import MView from './View'
import BottomMenu from './BottomMenu'


export default class WizardLayout extends Component{
  render(){
    const { children, showMenu = true } = this.props
    const { LogoWithText } = Logo
    const wizardStyles = StyleSheet.create({
      BottomMenuTitle: {
        color: '#2B6171',
        fontFamily: 'SansitaOne',
        fontSize: 25,
        marginLeft: 90
      }
    })
    return (
      <MView fluid justify="flex-end" background="#2B6171">
        <StatusBar barStyle="light-content" backgroundColor="#2B6171"/>
        <Transition appear="horizontal">
          <ScrollView contentContainerStyle={{flexGrow: 1}} style={{flex: 1}} scrollEnabled={true}>
            <MView style={{flex:1}} width="80%"  selfAlign="center" justify="flex-end">
              <LogoWithText />
            </MView>
            <MView style={{flex:1}} direction="column">
              { children }
            </MView>
          </ScrollView>
        </Transition>
        {showMenu ? 
            <BottomMenu>
              <Text style={wizardStyles.BottomMenuTitle}>Menu</Text>
            </BottomMenu>
         : null}
      </MView>
    )
  }
}