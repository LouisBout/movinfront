import React, { Component, Fragment } from 'react'
import { ListItem, MView } from 'components'
import styled from 'styled-components'
import { Text } from 'react-native'
import { Paragraph as PaperParagraph } from 'react-native-paper'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { styles } from 'assets/styles'


class OfferListItem extends Component {
  render() {
    const { offer } = this.props
    const Paragraph = styled(PaperParagraph)`
      font-family: Kameron-Regular;
      font-weight: 500;
      line-height: 22px;
      font-size: 16px;
      color: #2B6171;
    `
    const Footer = () => (
      <MView direction="row" justify='space-around'>
        <MView width="auto" direction="row">
          <Text style={{ color: '#D9EDEE', fontSize: 20, fontFamily: 'SansitaOne' }}>{offer.remuneration}</Text>
          <Icon name="euro-symbol" size={26} color="#F6DE18" />
        </MView>
        <MView width="auto" direction="row">
          <Text style={{ color: '#F6DE18', fontSize: 26, fontFamily: 'SansitaOne' }}>{offer.remunerationType}</Text>
        </MView>
      </MView>
    )
    return (
      <ListItem icon="transfer-within-a-station" title={offer.city.address || offer.city.description} footer={<Footer />} type="tertiary">
        <MView>
          <Paragraph numberOfLines={3}>
            {offer.description}
          </Paragraph>
        </MView>
      </ListItem>
    )
  }
}
export default OfferListItem