import React, { Component, Fragment } from 'react'
import { withNavigation } from 'react-navigation'
import { IconButton } from 'react-native-paper'
import { MView } from 'components'
import styled from 'styled-components/native'

class BottomMenu extends Component {
  goTo(route){
    this.props.navigation.navigate(route)
  }
  render() {
    const { children, color = "#D9EDEE", height = "80px" } = this.props
    const ViewContainer = styled.View`
      display: flex;
      position: absolute;
      background-color: ${color};
      padding: 2px;
      height: ${height};
      bottom: 0;
      width: 100%;
      align-content: center
      justify-content: center;
      align-self: flex-end;
    `
    return (
      <Fragment>
        <ViewContainer {...this.props}>
          <MView style={{ flexWrap: 'nowrap', flexDirection: 'row', alignContent: 'center', alignItems: 'center' }}>
            <IconButton
              style={{ width: 50, height: 50 }}
              icon="menu"
              color="#2B6171"
              size={50}
              onPress={() => this.goTo('MenuOverlay')}
            />
            {children}
          </MView>
        </ViewContainer>
      </Fragment>
    )
  }
}
export default withNavigation(BottomMenu)