import React from 'react'
import MImage from './Image'
import WithText from 'assets/img/logo-with-text.png'

export const LogoWithText = () => <MImage height="215px" width="285px" source={WithText}></MImage>