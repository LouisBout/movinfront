import React, { Component, Fragment } from 'react'
import { AsyncStorage, Keyboard } from 'react-native'
import gql from 'graphql-tag'
import { withNavigation } from 'react-navigation'
import { graphql } from 'react-apollo'
import { compose, withHandlers } from 'recompose'
import { login, me } from 'store/auth/schema.gql'
import { MView, MButton } from 'components'
import { TextInput, HelperText } from 'react-native-paper'
import { Formik } from 'formik'
import client from '@/client'
import { logInValidation } from 'validations'

const withData = compose(
  graphql(login),
  withHandlers({
    login: props => async event => {
        const { data } = await props.mutate(logInValidation.cast(event))
        await AsyncStorage.setItem('@payload', JSON.stringify(data.login))
        client.resetStore()
        const routeFrom = props.navigation.getParam('origin')
        if(routeFrom){
          props.navigation.navigate(routeFrom)
        }else{
          props.navigation.navigate('Home')
        }
    }
  })
)

class LogInForm extends Component {
  render() {
    const { login } = this.props
    return (
      <Formik
        initialValues={{ email: '', password: '' }}
        validationSchema={logInValidation}
        onSubmit={values => {
          login({ variables: values })
          Keyboard.dismiss()
        }
        }>
        {({ handleChange, handleSubmit, values, errors }) => (
          <Fragment>
            <TextInput
              autoCapitalize = 'none'
              keyboardType={'email-address'}
              error={errors.email}
              onChangeText={handleChange('email')}
              value={values.email}
              label="Email"
              placeholder="Entrez un email"
            />
            <HelperText
              type="error"
              visible={errors.email}
            >
              {errors.email}
            </HelperText>
            <TextInput
              error={errors.password}
              secureTextEntry={true}
              onChangeText={handleChange('password')}
              value={values.password}
              label="Mot de passe"
            />
            <HelperText
              type="error"
              visible={errors.password}
            >
              {errors.password}
            </HelperText>
            <MView height="75px" selfAlign="center">
              <MButton onPress={handleSubmit} background="tertiary" color="primary">Se connecter</MButton>
            </MView>
          </Fragment>
        )}
      </Formik>
    )
  }
}

export default withNavigation(withData(LogInForm))