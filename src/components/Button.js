import React, { Component } from 'react'
import styled from 'styled-components/native'

export default class MButton extends Component {
  render() {
    const { children, fontSize = '26px', background='secondary', color='primary', height = "100%", width = 'auto' } = this.props
    const bgColor = {
      primary: '#2B6171',
      secondary: '#F6DE18',
      tertiary: '#D9EDEE',
      transparent: 'transparent'
    }
    const BtnContainer = styled.TouchableOpacity`
      display: flex;
      background-color: ${bgColor[background]};
      padding: 2px;
      width: ${width};
      height: ${height};
      justify-content: center;
      border-radius: 5px;
    `
    const labelColor = {
      primary: '#2B6171',
      secondary: '#F6DE18',
      tertiary: '#D9EDEE',
      transparent: 'transparent'
    }

    const Label = styled.Text`
      color:  ${labelColor[color]};
      align-self: center;
      font-family: "SansitaOne";
      font-size: ${fontSize};
    `
    return (
      <BtnContainer {...this.props}>
        <Label>{children}</Label>
      </BtnContainer>
    )
  }
}