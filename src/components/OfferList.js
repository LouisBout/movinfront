import React, { Component } from 'react'
import { OfferListItem, MView } from 'components'


class OfferList extends Component {
  render() {
    const { offers } = this.props
    return (
      <MView fluid>
    {offers && offers.map((offer, idx) => <OfferListItem key={idx} offer={offer} />)}
      </MView>
    )
  }
}
export default OfferList