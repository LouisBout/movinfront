import React, { Component } from 'react'
import { AskListItem, MView } from 'components'


class AskList extends Component {
  render() {
    const { asks } = this.props
    return (
      <MView fluid>
    {asks && asks.map((ask, idx) => <AskListItem key={idx} ask={ask} />)}
      </MView>
    )
  }
}
export default AskList