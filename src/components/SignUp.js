import React, { Component, Fragment } from 'react'
import { AsyncStorage, Keyboard } from 'react-native'
import { withNavigation } from 'react-navigation'
import { graphql } from 'react-apollo'
import { compose, withHandlers } from 'recompose'
import { signup } from 'store/auth/schema.gql'
import { MView, MButton } from 'components'
import client from '@/client'
import { TextInput, HelperText } from 'react-native-paper'
import { Formik } from 'formik'
import { signUpValidation } from 'validations'


const withData = compose(
  graphql(signup),
  withHandlers({
    signup: props => async event => {
      const { data } = await props.mutate(event)
      await AsyncStorage.setItem('@payload', JSON.stringify(data.signup))
      client.resetStore()
      const routeFrom = props.navigation.getParam('origin')
      if(routeFrom){
        props.navigation.navigate(routeFrom)
      }else{
        props.navigation.navigate('Home')
      }
    }
  })
)

class SignUpForm extends Component {
  render() {
    const { signup } = this.props
    return (
      <Formik
        initialValues={{ email: '', password: '', confirmPassword: '' }}
        validationSchema={signUpValidation}
        onSubmit={values => {
          signup({ variables: values })
          Keyboard.dismiss()
        }
        }>
        {({ handleChange, handleSubmit, values, errors }) => (
          <Fragment>
            <TextInput
              autoCapitalize = 'none'
              error={errors.email}
              keyboardType={'email-address'}
              onChangeText={handleChange('email')}
              value={values.email}
              label="Email"
              placeholder="Entrez un email"
            />
            <HelperText
              type="error"
              visible={errors.email}
            >
              {errors.email}
            </HelperText>
            <TextInput
              error={errors.password}
              secureTextEntry={true}
              onChangeText={handleChange('password')}
              value={values.password}
              label="Mot de passe"
            />
            <HelperText
              type="error"
              visible={errors.password}
            >
              {errors.password}
            </HelperText>
            <TextInput
              errors={errors.confirmPassword}
              secureTextEntry={true}
              onChangeText={handleChange('confirmPassword')}
              value={values.confirmPassword}
              label="Confirmez le mot de passe"
            />
            <HelperText
              type="error"
              visible={errors.confirmPassword}
            >
              {errors.confirmPassword}
            </HelperText>
            <MView height="75px" selfAlign="center">
              <MButton onPress={handleSubmit} background="tertiary" color="primary">S'enregistrer</MButton>
            </MView>
          </Fragment>
        )}
      </Formik>
    )
  }
}

export default withNavigation(withData(SignUpForm))