import React, { Component } from 'react'
import styled from 'styled-components/native'

export default class MView extends Component{
  render(){
    const { children, display, position, background, height, width, direction, justify, alignItems, selfAlign, fluid } = this.props
    const ViewContainer = styled.View`
      display: ${display ? display : 'flex;'};
      background-color: ${background || 'transparent;'};
      height: ${height ? height: '100%;'};
      width: ${width ? width: '100%;'};
      ${justify ? `justify-content: ${justify};` : null}
      ${direction ? `flex-direction: ${direction};`:null};
      ${alignItems ? `align-items: ${alignItems};`:null};
      ${selfAlign ? `align-self: ${selfAlign};`:null};
      padding: ${fluid ? '0' : '5px'};
    `
    return (
      <ViewContainer {...this.props}>
        {children}
      </ViewContainer>
    )
  }
}