import React, { Component } from 'react'
import styled from 'styled-components/native'

export default class MImage extends Component{
  render() {
    const { width, height, source } = this.props
    const ImgContainer = styled.Image`
      width: ${width};
      height: ${height};
    `
    return <ImgContainer source={source} />
  }
}