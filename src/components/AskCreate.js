import React, { Component, Fragment } from 'react'
import { Keyboard, StyleSheet } from 'react-native'
import { graphql } from 'react-apollo'
import { TextInput, HelperText, Portal, Dialog, IconButton } from 'react-native-paper'
import { Formik } from 'formik'
import { withNavigation } from 'react-navigation'
import { compose, withHandlers, withState } from 'recompose'
import { createAsk } from 'store/ask/schema.gql'
import { me } from 'store/auth/schema.gql'
import { MView, MButton, MSwitch } from 'components'
import client from '@/client'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import { createAskValidation } from 'validations'




const withData = compose(
  graphql(createAsk),
  graphql(me, { name: 'me' }),
  withState('visible', 'setVisible', false),
  withState('route', 'redirectTo', 'LogIn'),
  withHandlers({
    createAsk: props => async event => {
      if (props.me.error) {
        props.setVisible(true)
      } else {
        const isValid = await createAskValidation.isValid(event)
        if (isValid) {
          props.mutate({
            variables: {
              data: {
                ...createAskValidation.cast(event), // Transform inputs
                author: {
                  connect: { id: props.me.me.id }
                }
              }
            }
          })
          client.resetStore()
          props.navigation.navigate('MySpaceHome', { module: 1 })
        }
      }
    },
    redirectTo: props => event => {
      props.navigation.navigate(event, { origin: 'AskPublish' })
      setTimeout(() => {
        props.setVisible(false)
      }, 300)
    }
  })
)

const styles = StyleSheet.create({
  submitButton: {
    marginTop: 16
  },
  dialogContainer: {
    backgroundColor: '#D9EDEE'
  },
  dialogClose: {
    width: 60,
    height: 60,
    alignSelf: 'flex-end'
  },
  dialogButtons: {
    margin: 10
  }
})


class AskCreate extends Component {
  render() {
    const { visible, setVisible, createAsk, redirectTo } = this.props
    return (
      <Fragment>
        <Formik
          initialValues={{ description: '', remunerationType: 'Forfait', peopleNeeded: null, remuneration: null, city: '' }}
          validationSchema={createAskValidation}
          onSubmit={values => {
            createAsk(values)
            Keyboard.dismiss()
          }
          }>
          {({ handleChange, handleSubmit, values, setFieldValue, errors }) => (
            <Fragment>
              <GooglePlacesAutocomplete
                placeholder='Entrez une ville'
                placeholderTextColor='#6e6e6e'
                minLength={2}
                autoFocus={false}
                returnKeyType={'search'}
                keyboardAppearance={'light'}
                listViewDisplayed='false'
                fetchDetails={true}
                onPress={(data, details = null) => {
                  setFieldValue('city', { create: { ...details.geometry.location, address: data.formatted_address || data.description } })
                }}
                query={{
                  key: 'AIzaSyD2hYIZydK-HkghQ8jCag8GO-na8PLR5Ic',
                  language: 'fr',
                  types: '(cities)'
                }}
                styles={{
                  container: {
                    marginBottom: 15
                  },
                  textInputContainer: {
                    height: 60,
                    backgroundColor: '#eeeeee',
                    flex: 1,
                    flexGrow: 1,
                    borderRadius: 5,
                    width: '100%',
                    borderTopWidth: 0,
                    borderBottomWidth: 0
                  },
                  textInput: {
                    height: 60,
                    borderTopWidth: 0,
                    backgroundColor: '#eeeeee',
                    marginTop: 0,
                    marginLeft: 0,
                    padding: 0
                  },
                  separator: {
                    backgroundColor: '#F6DE18',
                    alignSelf: 'center',
                    height: 2,
                    width: '80%'
                  },
                  description: {
                    color: 'white',
                    fontFamily: 'Kameron-Bold'
                  }
                }}
                enablePoweredByContainer={false}
                currentLocation={true}
                currentLocationLabel="Me géolocaliser"
                nearbyPlacesAPI='GoogleReverseGeocoding'
                GoogleReverseGeocodingQuery={{
                  result_type: 'locality'
                }}
                filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
                debounce={200}
              />
              <TextInput
                multiline={true}
                error={errors.description}
                style={{ marginBottom: 15 }}
                onChangeText={handleChange('description')}
                value={values.description}
                label="Description"
                placeholder="Entrez la description de votre annonce"
              />
              <MSwitch
                backgroundColor="#fff"
                buttonColor="#2B6171"
                globalColor="#2B6171"
                selectedColor="#F6DE18"
                style={{ marginBottom: 15 }}
                options={[
                  { label: "Forfait", value: "Forfait" },
                  { label: "Horaire", value: "Horaire" }]}
                onPress={value => setFieldValue('remunerationType', value)}
              />
              <TextInput
                keyboardType={'phone-pad'}
                error={errors.remuneration}
                onChangeText={handleChange('remuneration')}
                value={values.remuneration}
                label="Rémunération (€)"
                placeholder="Entrez le montant de la rémunération"
              />
              <HelperText
                type="error"
                visible={errors.remuneration}
              >
                {errors.remuneration}
              </HelperText>
              <TextInput
                keyboardType={'phone-pad'}
                error={errors.peopleNeeded}
                onChangeText={handleChange('peopleNeeded')}
                value={values.peopleNeeded}
                label="Nombre de personnes demandées"
                placeholder="Entrez le nombre de personnes demandées"
              />
              <HelperText
                type="error"
                visible={errors.peopleNeeded}
              >
                {errors.peopleNeeded}
              </HelperText>
              <MView height="50px" width="50%" fluid fontSize='12px'>
                <MButton onPress={handleSubmit}>Publier</MButton>
              </MView>
            </Fragment>
          )}
        </Formik>
        <Portal>
          <Dialog
            style={styles.dialogContainer}
            visible={visible}
          >
            <IconButton style={styles.dialogClose} icon="close" onPress={() => setVisible(false)} size={60} color="#2B6171" />
            <Dialog.Content>
              <MButton style={styles.dialogButtons}
                background="primary" color="tertiary"
                onPress={() => redirectTo('LogIn')}
                height='100px'>Se connecter</MButton>
              <MButton style={styles.dialogButtons}
                onPress={() => redirectTo('SignUp')}
                background="primary" color="tertiary"
                height='100px'>S'enregistrer</MButton>
            </Dialog.Content>
          </Dialog>
        </Portal>
      </Fragment>
    )
  }
}

export default withNavigation(withData(AskCreate))