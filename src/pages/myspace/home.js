import React, { Component } from 'react'
import { Divider, Title } from 'react-native-paper'
import { graphql } from 'react-apollo'
import { compose, withState, defaultProps } from 'recompose'
import { me } from 'store/auth/schema.gql'
import { ScrollView } from 'react-native'
import { MView, OfferList, AskList, BottomMenu, MSwitch } from 'components'
import { styles } from 'assets/styles'

const withData = compose(
  graphql(me),
  defaultProps({
    options: [
      {label: "Mes offres", value: 0},
      {label: "Mes demandes", value: 1}
    ]
  }),
  withState('selected', 'changeComponent',(props) => props.navigation.getParam('module'))
)
class MySpaceHome extends Component {
  render() {
    const { options, selected, changeComponent, data } = this.props
    return (
      <MView fluid background="#2B6171">
        <MView height="90%">
          <ScrollView contentContainerStyle={{paddingTop:100, paddingLeft:20, paddingRight:20}}>
            <Title style={styles.sectionTitle}>Mes annonces</Title>
            <Divider style={[styles.divider, {marginBottom: 30}]} />
            {selected == 0 ? <OfferList offers={data.me && data.me.offers} /> : <AskList asks={data.me && data.me.asks} />}
          </ScrollView>
        </MView>
        <BottomMenu>
          <MSwitch height={40} style={{width: '80%'}} initial={selected} options={options} onPress={value=>changeComponent(value)}/>
        </BottomMenu>
    </MView>
    )
  }
}
export default withData(MySpaceHome)