import React, { Component } from 'react'
import { AsyncStorage } from 'react-native'
import { MView, MButton, WizardLayout } from 'components'

export default class Homepage extends Component{
  goTo(route, params){
    this.props.navigation.navigate(route, params)
  }
  async isConnected(){
    return !! await AsyncStorage.getItem('@payload')
  }
  render(){
    return (
      <WizardLayout>
        <MView height="100px" width="80%" justify="center" selfAlign="center">
          <MButton onPress={() => this.goTo("WizardOffer")}>Apporter de l'aide</MButton>
        </MView>
        <MView height="100px" width="80%" justify="center" selfAlign="center">
          <MButton onPress={() => this.goTo("WizardAsk")}>Besoin de bras</MButton>
        </MView>
        <MView height="100px" width="80%" justify="center" selfAlign="center">
          <MButton onPress={async () => await this.isConnected() ? this.goTo("MySpaceHome", { module: 0 }) : this.goTo('LogIn')}>Espace perso</MButton>
        </MView>
      </WizardLayout>
    )
  }
}