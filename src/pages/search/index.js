import React, { Component } from 'react'
import { Divider, Title } from 'react-native-paper'
import { compose, withState, defaultProps } from 'recompose'
import { ScrollView } from 'react-native'
import { MView, OfferSearch, AskSearch, BottomMenu, MSwitch } from 'components'
import { styles } from 'assets/styles'

const withData = compose(
  defaultProps({
    options: [
      {label: "Offres", value: 0},
      {label: "Demandes", value: 1}
    ]
  }),
  withState('selected', 'changeComponent',(props) => props.navigation.getParam('module'))
)
class SearchPage extends Component {
  render() {
    const { options, selected, changeComponent } = this.props
    return (
      <MView fluid background="#2B6171">
        <MView height="90%">
          <ScrollView contentContainerStyle={{paddingTop:100, paddingLeft:20, paddingRight:20}}>
            <Title style={styles.sectionTitle}>Rechercher une annonce</Title>
            <Divider style={[styles.divider, {marginBottom: 30}]} />
            {selected == 0 ? <OfferSearch /> : <AskSearch />}
          </ScrollView>
        </MView>
        <BottomMenu>
          <MSwitch height={40} style={{width: '80%'}} initial={selected} options={options} onPress={value=>changeComponent(value)}/>
        </BottomMenu>
    </MView>
    )
  }
}
export default withData(SearchPage)