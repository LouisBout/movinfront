import React, { Component } from 'react'
import { Text, ScrollView} from 'react-native'
import { Title, Divider } from 'react-native-paper'
import { MView, BottomMenu, OfferCreate } from 'components'
import { styles } from 'assets/styles'


export default class OfferPublish extends Component{
  goTo(route){
    this.props.navigation.navigate(route)
  }
  
  render(){
    return (
      <MView fluid background="#2B6171">
        <MView height="90%">
          <ScrollView contentContainerStyle={{paddingTop:100, paddingLeft:20, paddingRight:20}}>
            <Title style={styles.sectionTitle}>Publier une annonce</Title>
            <Divider style={[styles.divider, {marginBottom: 30}]} />
            <OfferCreate />
          </ScrollView>
        </MView>
          <BottomMenu>
            <Text style={styles.BottomMenuTitle}>J'offre mon aide</Text>
          </BottomMenu>
      </MView>
    )
  }
}