import React, { Component } from 'react'
import { MView, MButton, WizardLayout } from 'components'

export default class AskForHelpWizard extends Component{
  goTo(route){
    this.props.navigation.navigate(route, { module: 0 })
  }
  render(){
    return (
      <WizardLayout>
        <MView height="100px" width="80%" justify="center" selfAlign="center">
          <MButton onPress={() => this.goTo("AskPublish")}>Publier une annonce</MButton>
        </MView>
        <MView height="100px" width="80%" justify="center" selfAlign="center">
          <MButton onPress={() => this.goTo("Search")}>Consulter les annonces</MButton>
        </MView>
      </WizardLayout>
    )
  }
}