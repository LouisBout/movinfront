import OfferHelpWizard from './OfferHelp'
import AskForHelpWizard from './AskForHelp'

export const Wizard = {
  WizardOffer: OfferHelpWizard,
  WizardAsk: AskForHelpWizard
}