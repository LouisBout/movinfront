import React, { Component } from 'react'
import { Text, ScrollView } from 'react-native'
import { Transition } from 'react-navigation-fluid-transitions'
import { MView, BottomMenu, LogInForm, Logo } from 'components'
import { styles } from 'assets/styles'

export default class LogIn extends Component{
  goTo(route){
    this.props.navigation.navigate(route)
  }
  
  render(){
    const { LogoWithText } = Logo
    return (
      <MView fluid background="#2B6171">
        <Transition appear="horizontal">
          <ScrollView contentContainerStyle={{paddingTop:100}}>
            <MView style={{flex:1}} width="80%" selfAlign="center">
              <LogoWithText />
            </MView>
            <MView style={{marginTop: 20}} selfAlign="center" width="80%">
             <LogInForm />
            </MView>
          </ScrollView>
        </Transition>
        <BottomMenu>
          <Text style={styles.BottomMenuTitle}>Connexion</Text>
        </BottomMenu>
      </MView>
    )
  }
}