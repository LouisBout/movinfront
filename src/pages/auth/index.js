import LogIn from './login'
import SignUp from './signup'


export const Auth = {
  LogIn,
  SignUp
}