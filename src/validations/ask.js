import * as yup from 'yup'

export const createAskValidation = 
  yup.object().shape({
    remunerationType: yup.string().required("Un type de rémunération est requis"),
    remuneration: yup.number().required("Un montant de rémunération est requis"),
    peopleNeeded: yup.number().default(1).required("Un nombre de personnes demandées est requis")
  })