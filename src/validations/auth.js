import * as yup from 'yup'

export const signUpValidation = 
  yup.object().shape({
    email: yup.string().email('Le format n\'est pas de type email').required('Un email est requis'),
    password: yup.string().required('Un mot de passe est requis'),
    confirmPassword: yup
    .string()
    .required()
    .test('passwords-match', 'Les mots de passe ne correspondent pas', function(value) {
      return this.parent.password === value;
    }),
  })

  export const logInValidation = 
  yup.object().shape({
    email: yup.string().email().required('Le format n\'est pas de type email').required('Un email est requis'),
    password: yup.string().required('Un mot de passe est requis')
  })