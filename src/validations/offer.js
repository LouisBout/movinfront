import * as yup from 'yup'

export const createOfferValidation = 
  yup.object().shape({
    remunerationType: yup.string().required("Un type de rémunération est requis"),
    remuneration: yup.number().required("Un montant de rémunération est requis")
  })