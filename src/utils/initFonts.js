import { Font } from 'expo'

export default () => 
  Font.loadAsync({
    'SansitaOne': require('../assets/fonts/Sansita/SansitaOne.ttf'),
    'Kameron-Bold': require('../assets/fonts/Kameron/Kameron-Bold.ttf'),
    'Kameron-Regular': require('../assets/fonts/Kameron/Kameron-Regular.ttf'),
  })
