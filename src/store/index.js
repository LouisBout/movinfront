import { withClientState } from 'apollo-link-state'
import cache from '@/cache'
import { merge } from 'lodash'
import Auth from './auth'
import Ask from './ask'
import Offer from './offer'



export default withClientState({
  cache,
  resolvers: merge(Auth.resolvers, Ask.resolvers, Offer.resolvers),
  typeDefs: merge(Auth.typeDefs, Ask.typeDefs, Offer.typeDefs)
})