import resolvers from './resolvers'
import state from './state'

const authClient = {
  ...state,
  resolvers
}

export default authClient