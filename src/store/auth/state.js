import gql from 'graphql-tag'
import cache from '@/cache'

const typeDefs = gql`
  extend type User {
    name: String
  }
`
cache.writeData({
  data: {
  }
})
export default typeDefs