import resolvers from './resolvers'
import state from './state'

const offerClient = {
  ...state,
  resolvers
}

export default offerClient