import gql from 'graphql-tag'
import cache from '@/cache'

const typeDefs = gql`
  extend type Offer {
    name: String
  }
  extend type Query {
    offers: [Offer]
  }
`
cache.writeData({
  data: {
  }
})
export default typeDefs