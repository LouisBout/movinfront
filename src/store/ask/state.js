import gql from 'graphql-tag'
import cache from '@/cache'

const typeDefs = gql`
  extend type Ask {
    name: String
  }
  extend type Query {
    asks: [Ask]
  }
`
cache.writeData({
  data: {
  }
})
export default typeDefs