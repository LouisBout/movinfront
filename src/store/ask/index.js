import resolvers from './resolvers'
import state from './state'

const askClient = {
  ...state,
  resolvers
}

export default askClient