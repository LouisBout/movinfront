import { StyleSheet } from 'react-native'



export const styles = StyleSheet.create({
  BottomMenuTitle: {
    marginLeft: 40, 
    color: '#2B6171', 
    fontSize: 25, 
    fontFamily:'SansitaOne'
  },
  sectionTitle: {
    fontFamily: 'SansitaOne',
    fontSize: 30,
    color: '#D9EDEE'
  },
  divider: {
    backgroundColor: '#F6DE18',
    borderRadius: 5,
    height: 3
  },
  listItem: {
    backgroundColor: '#D9EDEE', 
    borderRadius:5
  },
  listItemIcon: {
    backgroundColor: '#2B6171', 
    borderRadius:5
  },
  listItemsurface: {
    elevation: 4,
    marginBottom:15,
    borderRadius: 5,
  },
})