import React from 'react'
import { Platform } from 'react-native'
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper'


const theme = {
  ...DefaultTheme,
  roundness: 4,
  // fonts: Platform.select({
  //   default:{
  //     regular: 'Kameron-Regular',
  //     medium: 'Kameron-Regular',
  //     light: 'Kameron-Regular',
  //     thin: 'Kameron-Regular',
  //   }
  // }),
  colors: {
    ...DefaultTheme.colors,
    primary: '#2B6171',
    accent: '#F6DE18',
  }
}

export default ({ children }) =>(
  <PaperProvider theme={theme}>
    {children}
  </PaperProvider>
)